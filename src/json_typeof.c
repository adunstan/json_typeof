

#include "postgres.h"

#include "fmgr.h"
#include "utils/builtins.h"
#include "utils/jsonapi.h"


PG_MODULE_MAGIC;

typedef  struct typeof_state {
	JsonLexContext *lex;
	JsonTokenType result;
} TypeofState;

static void 
json_typeof_astart (void *state)
{
  TypeofState *_state = (TypeofState *) state;
  if (_state->lex->lex_level == 0)
	_state->result = JSON_TOKEN_ARRAY_START;
}
static void 
json_typeof_ostart (void *state)
{
  TypeofState *_state = (TypeofState *) state;
  if (_state->lex->lex_level == 0)
	_state->result = JSON_TOKEN_OBJECT_START;
}
static void 
json_typeof_scalar(
     void *state, 
     char *token, 
     JsonTokenType tokentype)
{
  TypeofState *_state = (TypeofState *) state;
  if (_state->lex->lex_level == 0)
	_state->result = tokentype;
}

extern Datum json_typeof(PG_FUNCTION_ARGS);

PG_FUNCTION_INFO_V1(json_typeof);


Datum 
json_typeof(PG_FUNCTION_ARGS)
{
	text       *json = PG_GETARG_TEXT_P(0);
	TypeofState  *state;
	JsonLexContext *lex = makeJsonLexContext(json, false);
	JsonSemAction *sem;
	char *result = "";
	
	state = palloc0(sizeof(TypeofState));
	sem = palloc0(sizeof(JsonSemAction));
	state->lex = lex;
	sem->semstate = (void *) state;
	sem->object_start = json_typeof_ostart;
	sem->array_start = json_typeof_astart;
	sem->scalar = json_typeof_scalar;
	pg_parse_json(lex, sem);
	switch (state->result)
	{
		case JSON_TOKEN_ARRAY_START:
			result = "array";
			break;
		case JSON_TOKEN_OBJECT_START:
			result = "object";
			break;
		case JSON_TOKEN_STRING:
			result = "string";
			break;
		case JSON_TOKEN_TRUE:
		case JSON_TOKEN_FALSE:
			result = "bool";
			break;
		case JSON_TOKEN_NUMBER:
			result = "number";
			break;
		case JSON_TOKEN_NULL:
			result = "null";
			break;
		default:
			break;
	}
	PG_RETURN_TEXT_P(cstring_to_text(result));
}
