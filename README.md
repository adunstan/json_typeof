# json_typeof extension

This extension provides a json_typeof(json) function that demonstrates a 
simple use of the JSON API in Postgres.

See the test files for how it works.

This was constructed as an adjunct to my talk at Postgres open 2013.
